package com.gkemayo.producer.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gkemayo.producer.model.Order;


@RestController
@RequestMapping("/producer")
@ConfigurationProperties
public class ProducerController {
	
	private static final String ORDER_SENDING_DATE = "ORDER_SENDING_DATE";

	private static final String KO = "KO - ";

	private static final String OK = "OK";

	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Value("${spring.jms.template.default-destination}")
	private String orderQueue;
	
	@PostMapping("/send/order")
	public ResponseEntity<String> submitOrder(@RequestBody Order order){
		
		try {
			
			//we serialize the LocalDateTime in String Json format to easily deserialize it in the consumer app
			String dateTimeStr = mapper.writeValueAsString(LocalDateTime.now());
			
			jmsTemplate.convertAndSend(orderQueue, order, message -> {
				message.setStringProperty(ORDER_SENDING_DATE, dateTimeStr);
				return message;
			});
			
			return new ResponseEntity<String>(OK, HttpStatus.OK);
		} catch (JmsException | JsonProcessingException e  ) {
			return new ResponseEntity<String>(KO + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}

}
