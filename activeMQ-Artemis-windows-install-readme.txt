Installation du Serveur/Broker ActiveMQ Artemis sur Windows
-----------------------------------------------------------

1- Télécharger ActiveMQ Artemis à l'adresse https://activemq.apache.org/components/artemis/download/ et le deposer dans un repertoire que nous nommons ${ARTEMIS_HOME}

2- Créer un repertoire sur la machine qui contiendra le Broker. Exemple : ${PATH}/server

3- Exécuter les commandes suivantes :

     3.1) se placer dans le repertoire server : cd ${PATH}/server
     3.2) création d'un Broker appelé appbroker : ${ARTEMIS_HOME}/bin/artemis create --user test --password test1 --host localhost --allow-anonymous n --force --verbose appbroker
     3.3) Ouvrir le fichier ${PATH}/server/appbroker/bin/artemis-service.xml pour éditer la Java Heap Space qui nous convient (Xms, Xmx)
     3.4) Démarrer le broker appbroker : 
              - soit par la commande ${PATH}/server/appbroker/bin/artemis.cmd run    Le broker s'arrêtera lorsqu'on éteint la machine
              - soit en intégrant l'exécution permanente dans les processus windows : ${PATH}/server/appbroker/bin/artemis-service.exe start

4- Visualiser la console navigateur ActiveMQ Artemis sur l'url http://localhost:8161/console/
      ==> la configuration de cette url se trouve dans ${PATH}/server/appbroker/etc/bootstrap.xml
      
5- De façon globale le fichier ${PATH}/server/appbroker/etc/broker.xml est le fichier contenant les paramétrages principaux du Broker.
