package com.gkemayo.consumer.service;

import java.time.LocalDateTime;

import javax.jms.JMSException;
import javax.jms.Message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gkemayo.consumer.model.OrderMessage;
import com.gkemayo.consumer.repository.ConsumerRepository;

@Service
public class ConsumerService {
	
	private static final String ORDER_SENDING_DATE = "ORDER_SENDING_DATE";

	@Autowired
	ConsumerRepository consumerRepository;
	
	@Autowired
	private ObjectMapper mapper;
	
	@JmsListener(destination = "${spring.jms.template.default-destination}")
	public void receiveOrder(@Payload OrderMessage orderMessage,  Message message) throws JMSException, JsonMappingException, JsonProcessingException, InterruptedException {
		
		String dateTimeJsonStr = message.getStringProperty(ORDER_SENDING_DATE);
		orderMessage.setSendingDate(mapper.readValue(dateTimeJsonStr, LocalDateTime.class));
		
		//simulate a long time processing orderMessage : 3 seconds
		Thread.sleep(3000);
		
		//then we archive this orderMessage in the database
		consumerRepository.save(orderMessage);
		
		//...notify the customer that the treatment of his orderMessage has been done: via an email for example.
	}

}
