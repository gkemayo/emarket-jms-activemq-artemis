package com.gkemayo.consumer.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@Entity
public class OrderMessage implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonInclude(value = Include.NON_ABSENT)
	private Long id; 
	
	private String customerName;
	
	private String customerEmail;
	
	private String customerAddress;
	
	private LocalDateTime sendingDate;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "order_id",referencedColumnName = "id")
	List<Item> itemList = new ArrayList<>();
	

}
