package com.gkemayo.consumer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gkemayo.consumer.model.OrderMessage;

@Repository
public interface ConsumerRepository extends JpaRepository<OrderMessage, Long>{

}
