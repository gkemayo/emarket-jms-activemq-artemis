package com.gkemayo.consumer;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.gkemayo.consumer.model.OrderMessage;

@SpringBootApplication
public class OrderConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderConsumerApplication.class, args);
	}
	
	@Bean
	public ObjectMapper mapper() {
		return new ObjectMapper().registerModule(new JavaTimeModule()).disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
	}
	
	@Bean
    public MessageConverter jacksonMessageConverter() {
        MappingJackson2MessageConverter jacksonConverter = new MappingJackson2MessageConverter();
        Map<String, Class<?>> typeMapping = new HashMap<String, Class<?>>();
        typeMapping.put("SHARED_TYPE", OrderMessage.class);
        jacksonConverter.setTypeIdMappings(typeMapping);
        jacksonConverter.setTargetType(MessageType.TEXT);
        jacksonConverter.setTypeIdPropertyName("_type");
        return jacksonConverter;
    }
	
}
